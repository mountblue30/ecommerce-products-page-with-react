// React
import React, { Component } from 'react';


// Components
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Category from './components/Category';


class App extends Component {
    state = {  } 
    render() { 
        return (
          <React.Fragment>
            <Navbar />
            <Category/>
            <Footer />
          </React.Fragment>
        );
    }
}
 
export default App;