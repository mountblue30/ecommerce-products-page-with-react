import React, { Component } from 'react';
import SkeletonCard from './SkeletonCard';


class SkeletonTemplate extends Component {
    state = {  } 
    render() { 
        return (
          <div
            className="section-one-part-two"
            style={{
              width:"100%"
            }}
          >
            <div className="wrap" id="wrap">
              <div className="title">
                <div className="skeleton skeleton-text"></div>
              </div>
              <div className="contents">
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
              </div>
            </div>
          </div>
        );
    }
}
 
export default SkeletonTemplate;