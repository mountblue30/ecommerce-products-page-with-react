import React, { Component } from "react";
import ProductTemplate from "./ProductTemplate";
import axiosBaseUrl from "./AxiosBaseUrl";

class FetchProducts extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount(){
    this.fetchData("products").then(
            (data) => {
              this.props.changeData(data);
            }
          )
  }
  fetchData(API) {
    return axiosBaseUrl
      .get(API)
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        this.props.changeError();
      });
      
  }

  groupDataBasedOnCategory(items) {
    return items.reduce((acc, item) => {
      if (acc[item.category]) {
        acc[item.category].push(item);
      } else {
        acc[item.category] = [item];
      }
      return acc;
    }, {});
  }

  render() {
    return (
      <>
        {!this.props.errorState && this.props.productState && (
          <ProductTemplate
            getProduct={this.props.getProduct}
            data={this.groupDataBasedOnCategory(this.props.products)}
          />
        )}
      </>
    );
  }
}

export default FetchProducts;
