import React, { Component } from "react";
import Popup from "./Popup";
import validator from "validator";
class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleError: "",
      descriptionError: "",
      priceError: "",
      ratingError: "",
    };
  }
  handleSubmit = (event) => {
    event.preventDefault();
    if (this.validate()) {
      this.props.togglePopup();
    }
  };
  handleInputChange = (event) => {
    const target = event.target;
    let value = target.value;
    const name = target.name;
    if (name.includes("title")) {
      this.props.product_title(event);
    }
    if (name.includes("description")) {
      this.props.product_description(event);
    }
    if (name.includes("price")) {
      this.props.product_price(event);
    }
    if (name.includes("image")) {
      this.props.product_image(event);
    }
  };
  validateImage = (image) => {
    return /^https?:\/\/.*\/.*\.(png|gif|webp|jpeg|jpg)\??.*$/gim.test(image);
  };
  validate = () => {
    let titleError = "";
    let descriptionError = "";
    let priceError = "";
    let imageError = "";

    if (validator.isEmpty(this.props.product.title)) {
      titleError = "Title field is required";
    }
    if (validator.isEmpty(this.props.product.description)) {
      descriptionError = "Description field is required";
    } else {
      if (!validator.isLength(this.props.product.description, { min: 50 })) {
        descriptionError = "Description must have atleast 50 characters.";
      }
    }
    if (validator.isEmpty(`${this.props.product.price}`)) {
      priceError = "Price field is required";
    } else {
      if (
        !validator.isFloat(`${this.props.product.price}`) &&
        !validator.isInt(`${this.props.product.price}`)
      ) {
        priceError = "Price should be of number type.";
      }
    }
    if (!this.props.product.image) {
      imageError = "Image should not be empty.Please provide a valid URL.";
    } else {
      if (!validator.isURL(this.props.product.image, {
          protocols: ["http", "https"],
        })) {
        imageError = "Provide a valid image url.";
      }
    }
    if (titleError || descriptionError || priceError || imageError) {
      this.setState({ titleError, descriptionError, priceError, imageError });
      return false;
    }
    this.setState({ titleError, descriptionError, priceError, imageError });
    return true;
  };
  render() {
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <h1> Edit Product </h1>
          <label htmlFor="title">Title:</label>
          <input
            type="text"
            id="product_title"
            name="product_title"
            value={this.props.product.title}
            onChange={this.handleInputChange}
          />
          <div className="errorMsg">{this.state.titleError}</div>

          <label htmlFor="description">Description:</label>
          <textarea
            id="product_description"
            name="product_description"
            value={this.props.product.description}
            onChange={this.handleInputChange}
          ></textarea>
          <div className="errorMsg">{this.state.descriptionError}</div>

          <label htmlFor="price">Price:</label>
          <input
            type="text"
            id="product_price"
            name="product_price"
            value={this.props.product.price}
            onChange={this.handleInputChange}
          />
          <div className="errorMsg">{this.state.priceError}</div>
          <label htmlFor="price">Image:</label>
          <input
            type="text"
            id="product_image"
            name="product_image"
            value={this.props.product.image}
            onChange={this.handleInputChange}
          />
          <div className="errorMsg">{this.state.imageError}</div>
          <button>Submit</button>
          {this.props.showPopup ? (
            <Popup
              cancelUpdate={this.props.cancelUpdate}
              updateConfirmation={this.props.updateConfirmation}
              togglePopup={this.props.togglePopup}
              updateConfirmationState={this.props.updateConfirmationState}
            />
          ) : null}
        </form>
      </>
    );
  }
}

export default Form;
