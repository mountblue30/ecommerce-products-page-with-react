import React, { Component } from "react";
import companyLogo from "../images/ecomwents-logo.png";
import rightBracketLogo from "../images/right-to-bracket-solid.svg";

function Navbar() {
  return (
    <nav>
      <img src={companyLogo} alt=""/>
    </nav>
  );
}

export default Navbar;
