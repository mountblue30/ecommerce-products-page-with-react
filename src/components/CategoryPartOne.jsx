import React, { Component } from "react";
import SkeletonTemplate from "./SkeletonTemplate";
import FetchProducts from "./FetchProducts";

class CategoryPartOne extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        {!this.props.dataState && <SkeletonTemplate />}
        {this.props.errorState && (
          <div className="errorMessage">
            <h1>An error occured while fetching products!</h1>
          </div>
        )}
        {this.props.dataState &&
          !this.props.productState &&
          !this.props.errorState &&(
            <div className="errorMessage">
              <h1>No products found!</h1>
            </div>
          )}
        {!this.props.errorState && (
          <FetchProducts
            changeData={this.props.changeData}
            changeError={this.props.changeError}
            productState={this.props.productState}
            errorState={this.props.errorState}
            products={this.props.products}
            getProduct={this.props.getProduct}
          />
        )}
      </>
    );
  }
}

export default CategoryPartOne;
