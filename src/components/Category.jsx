import React, { Component } from "react";
import CategoryPartOne from "./CategoryPartOne";
import CategoryPartTwo from "./CategoryPartTwo";

class Category extends Component {
  state = {
    dataState: false,
    errorState: false,
    productState: false,
    showPopup: false,
    updateConfirmation: false,
    formStatus: false,
    products: "",
    productId: "",
    currentProduct: "",
    temporaryProduct: {},
  };
  updateProductDetails = () => {
    this.setState((prevState) => ({
      products: prevState.products.map((product) =>
        product.id === this.state.productId
          ? {
              ...product,
              title: this.state.temporaryProduct["title"],
              description: this.state.temporaryProduct["description"],
              price: this.state.temporaryProduct["price"],
              rating: this.state.temporaryProduct["rating"],
              image: this.state.temporaryProduct["image"],
            }
          : product
      ),
      productId: "",
    }));
  };
  updateConfirmationState = () => {
    this.setState({
      updateConfirmation: true,
    });
  };
  togglePopup = () => {
    if (this.state.showPopup) {
      this.setState((state)=>{
        return {
          showPopup: !state.showPopup,
          formStatus: false,
          updateConfirmation:false,
        };
      },()=>{
        this.updateProductDetails();
      });
      
    } else {
      this.setState((state) => {
        return {
          showPopup: !state.showPopup,
        };
      });
    }
  };
  cancelUpdate = () => {
    this.setState({
      showPopup: !this.state.showPopup,
    });
  };
  changeDataState = (resultProducts) => {
    if (resultProducts.length > 0) {
      this.setState({
        dataState: true,
        productState: true,
        products: resultProducts,
      });
    } else {
      this.setState({
        dataState: true,
        productState: false,
      });
    }
  };

  changeErrorState = () => {
    this.setState({
      errorState: true,
    });
  };
  getProductId = (productId) => {
    this.setState((state) => {
      return {
        currentProduct: state.products[productId - 1],
        productId: productId,
        temporaryProduct: state.products[productId - 1],
        formStatus: true,
      };
    });
  };
  changeFormStatus = () => {
    this.setState({
      formStatus: false,
    });
  };
  handleTitle = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        title: event.target.value,
      },
    }));
  };
  handleDescription = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        description: event.target.value,
      },
    }));
  };
  handlePrice = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        price: event.target.value,
      },
    }));
  };
  handleImage = (event) => {
    this.setState((prevState) => ({
      temporaryProduct: {
        ...prevState.temporaryProduct,
        image: event.target.value,
      },
    }));
  };
  render() {
    return (
      <section className="section-one">
        <CategoryPartOne
          dataState={this.state.dataState}
          productState={this.state.productState}
          errorState={this.state.errorState}
          products={this.state.products}
          changeData={this.changeDataState}
          changeError={this.changeErrorState}
          getProduct={this.getProductId}
        />
        <CategoryPartTwo
          product={this.state.temporaryProduct}
          dataState={this.state.dataState}
          productState={this.state.productState}
          errorState={this.state.errorState}
          showPopup={this.state.showPopup}
          formStatus={this.state.formStatus}
          updateConfirmation={this.state.updateConfirmation}
          updateConfirmationState={this.updateConfirmationState}
          handleTitle={this.handleTitle}
          handleDescription={this.handleDescription}
          handlePrice={this.handlePrice}
          handleImage={this.handleImage}
          togglePopup={this.togglePopup}
          updateProductDetails={this.updateProductDetails}
          changeFormStatus={this.changeFormStatus}
          inputValidate={this.validate}
          cancelUpdate={this.cancelUpdate}
        />
      </section>
    );
  }
}

export default Category;
