import React, { Component } from 'react';


class Popup extends Component {
  constructor(props){
    super(props)
  }
  render() { 
    return (
      <div className="popup">
        {this.props.updateConfirmation && (
          <div className="popup_inner">
            <h1>Product has been updated successfully</h1>
            <button onClick={this.props.togglePopup}>Continue</button>
          </div>
        )}
        {!this.props.updateConfirmation && (
          <div className="popup_inner">
            <h1>Are you sure you want to update the product.</h1>
            <button onClick={this.props.updateConfirmationState}>Yes</button>
            <button onClick={this.props.cancelUpdate}>No</button>
          </div>
        )}
      </div>
    );
  }
}
 
export default Popup;