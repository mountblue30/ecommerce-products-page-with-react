import React, { Component } from 'react';

class ProductCards extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <React.Fragment>
        {this.props.products.map((product)=>{
          return (
            <a
              className="undefined"
              key={product.title}
              onClick={() => this.props.getProduct(product.id)}
            >
              <div className="card">
                <img src={product.image} />
                <div className="content-spacing">
                  <h3>{product.title}</h3>
                  <h4>${product.price}</h4>
                  <p>{product.description}</p>
                  <h4>Rating : {product.rating["rate"]}</h4>
                </div>
              </div>
            </a>
          );
        })}
      </React.Fragment>
    );
  }
}
export default ProductCards;
