import axios from "axios";

let axiosBaseUrl =  axios.create({
  baseURL: "https://fakestoreapi.com/",
});

export default axiosBaseUrl;