import React, { Component } from 'react';


class SkeletonCard extends Component {
    state = {  } 
    render() { 
        return (
          <a className="undefined">
            <div className="card">
              <img className="skeleton" />
              <div className="content-spacing">
                <div className="skeleton skeleton-text"></div>
                <div className="skeleton skeleton-text"></div>
                <div className="skeleton skeleton-text last"></div>
                <button>View Product</button>
              </div>
            </div>
          </a>
        );
    }
}
 
export default SkeletonCard;