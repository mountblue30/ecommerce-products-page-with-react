import React, { Component } from "react";
import Form from "./Form";
class CategoryPartTwo extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <div
        className="section-one-part-one"
        style={{
          display: !this.props.dataState ? "none" : "flex",
        }}
      >
        {!this.props.formStatus &&
          !this.props.errorState &&
          this.props.productState && (
            <div
              className="section-one-part-one-container"
              style={{
                backgroundColor: "white",
                textAlign: "center",
              }}
            >
              <p>Please select a product</p>
            </div>
          )}
        {this.props.formStatus && (
          <div className="section-one-part-one-container">
            <Form
              product={this.props.product}
              product_title={this.props.handleTitle}
              product_description={this.props.handleDescription}
              product_price={this.props.handlePrice}
              product_image={this.props.handleImage}
              togglePopup={this.props.togglePopup}
              showPopup={this.props.showPopup}
              updateProductDetails={this.props.updateProductDetails}
              changeFormStatus={this.props.changeFormStatus}
              updateConfirmationState={this.props.updateConfirmationState}
              updateConfirmation={this.props.updateConfirmation}
              cancelUpdate={this.props.cancelUpdate}
            />
          </div>
        )}
      </div>
    );
  }
}

export default CategoryPartTwo;
